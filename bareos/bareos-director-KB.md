<h1>How to install Bareos Director on Red Hat based Distros</h1>
<h2>add the Bareos repository</h2>
<pre>dnf install wget</pre>
<p>DIST=RHEL_8<br>
# or<br>
# DIST=RHEL_7<br>
# DIST=CentOS_8<br>
# DIST=CentOS_7<br>
# DIST=Fedora_33<br>
# DIST=Fedora_32<br>
# DIST=Fedora_31</p>
<p></p>
<p>RELEASE=release/20<br>
# RELEASE=experimental/nightly<br>
URL=https://download.bareos.org/bareos/$RELEASE/$DIST<br>
wget -O /etc/yum.repos.d/bareos.repo $URL/bareos.repo</p>
<p></p>
<h2>Install and Setup Postgresql</h2>
<p><pre>dnf module enable postgresql:13</pre><br>
<pre>dnf install postgresql-server</pre><br>
<pre>postgresql-setup --initdb</pre><br>
<pre>systemctl enable postgresql</pre><br>
<pre>sudo -i -u postgres</pre><br>
<pre>/usr/lib/bareos/scripts/create_bareos_database</pre><br>
<pre>/usr/lib/bareos/scripts/make_bareos_tables</pre><br>
<pre>/usr/lib/bareos/scripts/grant_bareos_privileges</pre></p>
<p></p>
<h2>Install BareOS</h2>
<pre>dnf install bareos bareos-database-postgresql</pre>
<p></p>
<h2># Install BareOS WebUI</h2>
<pre>dnf install bareos-webui</pre>
<pre>setsebool -P httpd_can_network_connect on</pre>
<p></p>
<h2>Configure Firewall</h2>
<pre>firewall-cmd --permanent --zone=public --add-service=http</pre>
<pre>firewall-cmd --permanent --zone=public --add-port=9001</pre>
<pre>firewall-cmd --permanent --zone=public --add-port=9101/tcp</pre>
<pre>firewall-cmd --permanent --zone=public --add-port=9103/tcp</pre>
<pre>firewall-cmd --reload</pre>
<p></p>
<h2>Enable Services</h2>
<pre>systemctl enable --now postgresql</pre>
<pre>systemctl enable --now httpd</pre>
<pre>systemctl enable --now bareos-fd</pre>
<pre>systemctl enable --now bareos-sd</pre>
<pre>systemctl enable --now bareos-dir</pre>
<p></p>
<h2>Setup WebUI User</h2>
<pre>bconsole</pre>
<pre>configure add console name=admin password=secret profile=webui-admin tlsenable=false</pre>

[Back](./readme.md)