folder="daily"
keep=2
DB="synapse"

echo "Cleaning up backups of $DB database..."

all=( `find "${BACKUP_DIR}/${folder}" -maxdepth 1 -name "${DB}-*" | sort -t/ -k3` )
files=()
number=$((${#all[@]}-$keep))

echo "Number of Backups to be deleted: $number"

if [[ $number -le 0 ]]
then

  echo "Only ${#all[@]} Backups exist for ${DB} and you want to keep $keep."
  echo "If you have just started taking backups you may ignore this"
  echo "Otherwise you may want to investigate why backups are not being taken"

elif [[ $number -gt 0 ]]
then

    "Removing Backups"

else

    echo $number

fi

echo "Test Finished"

